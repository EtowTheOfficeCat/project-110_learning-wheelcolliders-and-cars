﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour {

    public GameObject fracturedPrefab;

    private void OnMouseDown()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        Instantiate(fracturedPrefab, transform.position, transform.rotation);
        Destroy(this);
    }
}
