﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarPartsDamage : MonoBehaviour
{
    [SerializeField] SkinnedMeshRenderer carPart;
    [SerializeField] float Thrust = 100;
    [SerializeField] float Angle;

    private float DamageCounter = 0;
    private Rigidbody rb;
    private Collider carCollider;
    private HingeJoint joint;
    

    private void Update()
    {
        

        if (DamageCounter == 2)
        {
            carPart.SetBlendShapeWeight(0, 100f);
            
            
            HingeJoint joint = GetComponent<HingeJoint>();
            JointLimits limits = joint.limits;
            limits.min = Angle;
            limits.max = 1;
            limits.bounciness = 0;
            limits.bounceMinVelocity = 0;
            joint.limits = limits;

            
            
        }

        if(DamageCounter == 3)
        {
            HingeJoint joint = GetComponent<HingeJoint>();
            Destroy(joint);
            this.transform.parent = null;
            rb = GetComponent<Rigidbody>();
            rb.AddForce(transform.up * Thrust);
            rb.useGravity = true;
            carCollider = GetComponent<Collider>();
            carCollider.isTrigger = false;
           
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        DamageCounter += 1;
      
    }
}
