﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarDamage : MonoBehaviour
{
    public SkinnedMeshRenderer doorFR;
    float damageTime = 0.5f;
    float damageTimer = 0f;
    bool isDamaging = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            isDamaging = true;
            doorFR.SetBlendShapeWeight(0, 100f);
        }
        if (isDamaging)
        {
            damageTimer += Time.deltaTime;
            float t = damageTimer / damageTime;
            float blendShapeValue = Mathf.Lerp(0, 100, t);
            doorFR.SetBlendShapeWeight(0, blendShapeValue);

            if(damageTimer >= damageTime)
            {
                isDamaging = false;
            }
        }
    }
}
