using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RearWheelDriveWithGears : MonoBehaviour
{

    private WheelCollider[] wheels;

    public TextMesh tm;
    public AnimationCurve rpmCurve;
    public AnimationCurve gearCurve;
    public WheelCollider wheelBL;
    public WheelCollider wheelBR;
    public int minRPM = 1000;
    public float maxAngle = 30;
    public float maxTorque = 300;
    public GameObject wheelShape;
    private int curGear = 0;
    private float finalDriveRatio = 3.2f;
    private Rigidbody rb;

    // here we find all the WheelColliders down in the hierarchy
    public void Start()
    {
        wheels = GetComponentsInChildren<WheelCollider>();
        rb = GetComponent<Rigidbody>();

        for (int i = 0; i < wheels.Length; ++i)
        {
            var wheel = wheels[i];

            // create wheel shapes only when needed
            if (wheelShape != null)
            {
                var ws = GameObject.Instantiate(wheelShape);
                ws.transform.parent = wheel.transform;
            }
        }
    }

    // this is a really simple approach to updating wheels
    // here we simulate a rear wheel drive car and assume that the car is perfectly symmetric at local zero
    // this helps us to figure our which wheels are front ones and which are rear
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            curGear = Mathf.Clamp(--curGear, -1, 5);
        }
        else if (Input.GetKeyDown(KeyCode.I))
        {
            curGear = Mathf.Clamp(++curGear, -1, 5);
        }

        float angle = maxAngle * Input.GetAxis("Horizontal");
        //float torque = maxTorque * Input.GetAxis("Vertical");

        float wheelsRPM = (wheelBL.rpm + wheelBR.rpm) / 2f;
        float curRPM = minRPM + (wheelsRPM * finalDriveRatio * gearCurve.Evaluate(curGear));
        float torque = rpmCurve.Evaluate(curRPM) * gearCurve.Evaluate(curGear) * finalDriveRatio * Mathf.Clamp01(Input.GetAxis("Vertical"));
        float kmh = rb.velocity.magnitude * 3.6f;
        tm.text = $"Gear: {curGear}\nRPM: {curRPM.ToString("0")}\nSpeed: {kmh.ToString("000")} km/h";

        foreach (WheelCollider wheel in wheels)
        {
            // a simple car where front wheels steer while rear ones drive
            if (wheel.transform.localPosition.z > 0)
                wheel.steerAngle = angle;

            if (wheel.transform.localPosition.z < 0)
                wheel.motorTorque = torque / 2;

            // update visual wheels if any
            if (wheelShape)
            {
                Quaternion q;
                Vector3 p;
                wheel.GetWorldPose(out p, out q);

                // assume that the only child of the wheelcollider is the wheel shape
                Transform shapeTransform = wheel.transform.GetChild(0);
                shapeTransform.position = p;
                shapeTransform.rotation = q;
            }

        }
    }
}
