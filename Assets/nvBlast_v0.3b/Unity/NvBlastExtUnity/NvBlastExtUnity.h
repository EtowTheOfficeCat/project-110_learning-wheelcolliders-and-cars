#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include "windows.h"

#include "NvBlast.h"
#include "NvBlastAssert.h"
#include "NvBlastGlobals.h"
//#include "NvBlastExtExporter.h"
//#include "NvBlastPxCallbacks.h"
//#include "NvBlastTkAsset.h"
//#include "NvBlastExtLlSerialization.h"
//#include "NvBlastExtTkSerialization.h"
//#include "NvBlastExtPxSerialization.h"
#include "NvBlastExtAuthoring.h"
#include "NvBlastExtAuthoringMesh.h"
#include "NvBlastExtAuthoringBondGenerator.h"
#include "NvBlastExtAuthoringCollisionBuilder.h"
#include "NvBlastExtAuthoringCutout.h"
#include "NvBlastExtAuthoringFractureTool.h"
//#include "BlastDataExporter.h"
#include "SimpleRandomGenerator.h"
#include "NvBlastExtAuthoringMeshCleaner.h"
#include "NvBlastTkFramework.h"

//#include "Px.h"
#include "PxPhysicsAPI.h"
#include "PxFoundation.h"

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace Nv::Blast;
using namespace std;