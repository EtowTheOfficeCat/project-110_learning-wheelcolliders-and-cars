#include "NvBlastExtUnity.h"

//#define CONSOLEDEBUG

#pragma region GLOBAL_VARS
SimpleRandomGenerator rnd;

//physx::PxFoundation*	gFoundation = nullptr;
//physx::PxPhysics*		gPhysics = nullptr;
//physx::PxCooking*		gCooking = nullptr;
//
//Nv::Blast::BlastBondGenerator* bondGenerator = nullptr;
//Nv::Blast::ConvexMeshBuilder* collisionBuilder = nullptr;
//Nv::Blast::CollisionParams collisionParameter;
#pragma endregion

#pragma region GLOBAL_FUNCTIONS
NVBLAST_API void Version()
{
	printf("Unity Wrapper V1.0\n");
}
NVBLAST_API void setSeed(int seed)
{
	rnd.seed(seed);
}
#pragma endregion

#pragma region MESH_FUNCTIONS

/*
Mesh Virtual Functions
*/

NVBLAST_API void _Mesh_Relase(Mesh* mesh)
{
	if (!mesh) { printf("(Warning) _Mesh_Relase: NULL mesh\n"); return; }
	mesh->release();
}

NVBLAST_API void _Mesh_getVertices(Mesh* mesh, void* arr)
{
	if (!arr) { printf("(Warning) _Mesh_getVertices: NULL arr\n"); return; }
	if (!mesh) { printf("(Warning) _Mesh_getVertices: NULL mesh\n"); return; }
	const Vertex* verts = mesh->getVertices();

	physx::PxVec3* vertArr = (physx::PxVec3*)arr;

	for (uint32_t i = 0; i < mesh->getVerticesCount(); i++)
	{
		vertArr[i] = verts[i].p;
	}
}
NVBLAST_API void _Mesh_getNormals(Mesh* mesh, void* arr)
{
	if (!arr) { printf("(Warning) _Mesh_getNormals: NULL arr\n"); return; }
	if (!mesh) { printf("(Warning) _Mesh_getNormals: NULL mesh\n"); return; }
	const Vertex* verts = mesh->getVertices();

	physx::PxVec3* vertArr = (physx::PxVec3*)arr;

	for (uint32_t i = 0; i < mesh->getVerticesCount(); i++)
	{
		vertArr[i] = verts[i].n;
	}
}
NVBLAST_API void _Mesh_getIndexes(Mesh* mesh, void* arr)
{
	if (!arr) { printf("(Warning) _Mesh_getIndexes: NULL arr\n"); return; }
	if (!mesh) { printf("(Warning) _Mesh_getIndexes: NULL mesh\n"); return; }

	const Edge* edges = mesh->getEdges();
	const Facet* facet = mesh->getFacetsBuffer();

	uint32_t* vertArr = (uint32_t*)arr;

	for (uint32_t i = 0; i < mesh->getFacetCount(); i++)
	{
		vertArr[i * 3 + 0] = edges[facet[i].firstEdgeNumber + 0].s;
		vertArr[i * 3 + 1] = edges[facet[i].firstEdgeNumber + 1].s;
		vertArr[i * 3 + 2] = edges[facet[i].firstEdgeNumber + 2].s;
	}
}

NVBLAST_API void _Mesh_getUVs(Mesh* mesh, void* arr)
{
	if (!arr) { printf("(Warning) _Mesh_getUVs: NULL arr\n"); return; }
	if (!mesh) { printf("(Warning) _Mesh_getUVs: NULL mesh\n"); return; }
	const Vertex* verts = mesh->getVertices();

	physx::PxVec2* vertArr = (physx::PxVec2*)arr;

	for (uint32_t i = 0; i < mesh->getVerticesCount(); i++)
	{
		vertArr[i] = verts[i].uv[0];
	}
}

NVBLAST_API uint32_t _Mesh_getVerticesCount(Mesh* mesh)
{
	if (!mesh) { printf("(Warning) _Mesh_getVerticesCount: NULL mesh\n"); return 0; }
	return mesh->getVerticesCount();
}

NVBLAST_API uint32_t _Mesh_getIndexesCount(Mesh* mesh)
{
	if (!mesh) { printf("(Warning) _Mesh_getIndexesCount: NULL mesh\n"); return 0; }
	return mesh->getFacetCount() * 3;
}
#pragma endregion

#pragma region MESHCLEANER_FUNCTIONS

/*
MeshCleaner Virtual Functions
*/
NVBLAST_API void _Cleaner_Release(MeshCleaner* cleaner)
{
	if (!cleaner) { printf("(Warning) _Cleaner_Release: NULL MeshCleaner\n"); return; }
	cleaner->release();
}

NVBLAST_API Mesh* _Cleaner_cleanMesh(MeshCleaner* cleaner, Mesh* mesh)
{
	if (!mesh) { printf("(Warning) _Cleaner_cleanMesh: NULL mesh\n"); return NULL; }
	if (!cleaner) { printf("(Warning) _Cleaner_cleanMesh: NULL MeshCleaner\n"); return NULL; }
	return cleaner->cleanMesh(mesh);
}
#pragma endregion

#pragma region FRACTURETOOL_FUNCTIONS
/*
FractureTool Virtual Functions
*/
NVBLAST_API void _FractureTool_Release(FractureTool* tool)
{
	if (!tool) { printf("(Warning) _FractureTool_Release: NULL tool\n"); return; }
	tool->release();
}

NVBLAST_API void _FractureTool_setRemoveIslands(FractureTool* tool, bool remove)
{
	if (!tool) { printf("(Warning) _FractureTool_setMesh: NULL tool\n"); return; }
	tool->setRemoveIslands(remove);
}

NVBLAST_API void _FractureTool_setSourceMesh(FractureTool* tool, Mesh* mesh)
{
	if (!mesh) { printf("(Warning) _FractureTool_setMesh: NULL mesh\n"); return; }
	if (!tool) { printf("(Warning) _FractureTool_setMesh: NULL tool\n"); return; }
	tool->setSourceMesh(mesh);
}

NVBLAST_API bool _FractureTool_voronoiFracturing(FractureTool* tool, uint32_t chunkId, VoronoiSitesGenerator* vsg)
{
	if (!vsg) { printf("(Warning) _FractureTool_voronoiFracturing: NULL vsg\n"); return false; }
	if (!tool) { printf("(Warning) _FractureTool_voronoiFracturing: NULL tool\n"); return false; }

	const physx::PxVec3* sites = nullptr;
	uint32_t sitesCount = vsg->getVoronoiSites(sites);

	return tool->voronoiFracturing(chunkId, sitesCount, sites, false);
}

NVBLAST_API bool _FractureTool_slicing(FractureTool* tool, int chunkId, SlicingConfiguration* conf, bool replaceChunk)
{
	if (!tool) { printf("(Warning) _FractureTool_slicing: NULL tool\n"); return NULL; }

	return tool->slicing(chunkId, *conf, replaceChunk, &rnd);
}

NVBLAST_API void _FractureTool_finalizeFracturing(FractureTool* tool)
{
	if (!tool) { printf("(Warning) _FractureTool_finalizeFracturing: NULL tool\n"); return; }

	tool->finalizeFracturing();
}

//NVBLAST_API AuthoringResult* _FractureTool_finalizeWithResult(FractureTool* tool)
//{
//	if (!tool) { printf("(Warning) _FractureTool_finalizeFracturing: NULL tool\n"); return NULL; }
//
//	Nv::Blast::AuthoringResult* result = NvBlastExtAuthoringProcessFracture(*tool, *bondGenerator, *collisionBuilder, collisionParameter);
//
//	return result;
//}

NVBLAST_API uint32_t _FractureTool_getChunkCount(FractureTool* tool)
{
	if (!tool) { printf("(Warning) _FractureTool_getChunkCount: NULL tool\n"); return 0; }

	return tool->getChunkCount();
}

NVBLAST_API Mesh* _FractureTool_getChunkMesh(FractureTool* tool, int chunkId, bool inside)
{
	if (!tool) { printf("(Warning) _FractureTool_getChunkMesh: NULL tool\n"); return NULL; }

	//tool->getBufferedBaseMeshes()

	Triangle* tris;
	uint32_t s = tool->getBaseMesh(chunkId, tris);

	physx::PxVec3* pos = new physx::PxVec3[s * 3];
	physx::PxVec3* norm = new physx::PxVec3[s * 3];
	physx::PxVec2* uv = new physx::PxVec2[s * 3];
	//uint32_t* idx = new uint32_t[s * 3];
	vector<uint32_t> idx;

	for (uint32_t i = 0; i < s; i++)
	{
		pos[i * 3 + 0] = tris[i].a.p;
		pos[i * 3 + 1] = tris[i].b.p;
		pos[i * 3 + 2] = tris[i].c.p;

		norm[i * 3 + 0] = tris[i].a.n;
		norm[i * 3 + 1] = tris[i].b.n;
		norm[i * 3 + 2] = tris[i].c.n;

		uv[i * 3 + 0] = tris[i].a.uv[0];
		uv[i * 3 + 1] = tris[i].b.uv[0];
		uv[i * 3 + 2] = tris[i].c.uv[0];

		if (inside && tris[i].materialId == MATERIAL_INTERIOR)
		{
			idx.push_back(i * 3 + 0);
			idx.push_back(i * 3 + 1);
			idx.push_back(i * 3 + 2);
		}
		if (!inside && tris[i].materialId != MATERIAL_INTERIOR)
		{
			idx.push_back(i * 3 + 0);
			idx.push_back(i * 3 + 1);
			idx.push_back(i * 3 + 2);
		}
		//idx[i * 3 + 0] = i * 3 + 0;
		//idx[i * 3 + 1] = i * 3 + 1;
		//idx[i * 3 + 2] = i * 3 + 2;
	}
	return NvBlastExtAuthoringCreateMesh(pos, norm, uv, s * 3, idx.data(), idx.size());

	//return tool->createChunkMesh(chunkId);
}

#pragma endregion

#pragma region VORONOISITES_FUNCTIONS
/*
NvVoronoiSitesGenerator Virtual Functions
*/
NVBLAST_API void _NvVoronoiSitesGenerator_Release(VoronoiSitesGenerator* tool)
{
	if (!tool) { printf("(Warning) _NvVoronoiSitesGenerator_Release: NULL tool\n"); return; }

	tool->release();
}

NVBLAST_API VoronoiSitesGenerator* _VoronoiSitesGenerator_Create(Mesh* mesh)
{
	return NvBlastExtAuthoringCreateVoronoiSitesGenerator(mesh, &rnd);
}

NVBLAST_API void _NvVoronoiSitesGenerator_uniformlyGenerateSitesInMesh(VoronoiSitesGenerator* tool, int count)
{
	if (!tool) { printf("(Warning) _NvVoronoiSitesGenerator_uniformlyGenerateSitesInMesh: NULL tool\n"); return; }
	tool->uniformlyGenerateSitesInMesh(count);
}

NVBLAST_API void _NvVoronoiSitesGenerator_clusteredSitesGeneration(VoronoiSitesGenerator* tool, uint32_t numberOfClusters, uint32_t sitesPerCluster, float clusterRadius)
{
	if (!tool) { printf("(Warning) _NvVoronoiSitesGenerator_clusteredSitesGeneration: NULL tool\n"); return; }
	tool->clusteredSitesGeneration(numberOfClusters, sitesPerCluster, clusterRadius);
}

NVBLAST_API void _NvVoronoiSitesGenerator_addSite(VoronoiSitesGenerator* tool, physx::PxVec3* site)
{
	if (!tool) { printf("(Warning) _NvVoronoiSitesGenerator_addSite: NULL tool\n"); return; }
	tool->addSite(*site);
}

NVBLAST_API uint32_t _NvVoronoiSitesGenerator_getSitesCount(VoronoiSitesGenerator* tool)
{
	if (!tool) { printf("(Warning) _NvVoronoiSitesGenerator_getSiteCount: NULL tool\n"); return 0; }

	const physx::PxVec3* sites;
	return tool->getVoronoiSites(sites);
}

NVBLAST_API void _NvVoronoiSitesGenerator_getSites(VoronoiSitesGenerator* tool, void* arr)
{
	if (!tool) { printf("(Warning) _NvVoronoiSitesGenerator_getSiteCount: NULL tool\n"); return; }

	physx::PxVec3* vertArr = (physx::PxVec3*)arr;

	const physx::PxVec3* sites;
	uint32_t total = tool->getVoronoiSites(sites);
	for (uint32_t i = 0; i < total; i++)
	{
		vertArr[i] = sites[i];
	}
}

#pragma endregion

#pragma region INIT_FUNCTIONS
//NV_INLINE physx::PxAllocatorCallback& NvBlastGetPxAllocatorCallback()
//{
//	class PxAllocatorCallbackWrapper : public physx::PxAllocatorCallback
//	{
//		virtual void* allocate(size_t size, const char* typeName, const char* filename, int line) override
//		{
//			return NvBlastGlobalGetAllocatorCallback()->allocate(size, typeName, filename, line);
//		}
//
//		virtual void deallocate(void* ptr) override
//		{
//			NvBlastGlobalGetAllocatorCallback()->deallocate(ptr);
//		}
//	};
//	static PxAllocatorCallbackWrapper wrapper;
//	return wrapper;
//}
//
//NV_INLINE physx::PxErrorCallback& NvBlastGetPxErrorCallback()
//{
//	class PxErrorCallbackWrapper : public physx::PxErrorCallback
//	{
//		virtual void reportError(physx::PxErrorCode::Enum code, const char* message, const char* file, int line) override
//		{
//			NvBlastGlobalGetErrorCallback()->reportError((Nv::Blast::ErrorCode::Enum)code, message, file, line);
//		}
//	};
//	static PxErrorCallbackWrapper wrapper;
//	return wrapper;
//}
//
//bool Init()
//{
//	gFoundation = PxCreateFoundation(PX_FOUNDATION_VERSION, NvBlastGetPxAllocatorCallback(), NvBlastGetPxErrorCallback());
//	if (!gFoundation) { printf("Can't init PhysX foundation\n"); return false; }
//
//	physx::PxTolerancesScale scale;
//	gPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *gFoundation, scale, true);
//	if (!gPhysics) { printf("Can't create Physics\n"); return false; }
//
//	physx::PxCookingParams cookingParams(scale);
//	cookingParams.buildGPUData = true;
//	gCooking = PxCreateCooking(PX_PHYSICS_VERSION, gPhysics->getFoundation(), cookingParams);
//	if (!gCooking) { printf("Can't create Cooking\n"); return false; }
//
//	bondGenerator = NvBlastExtAuthoringCreateBondGenerator(gCooking, &gPhysics->getPhysicsInsertionCallback());
//	if (!bondGenerator) { printf("Can't create bondGenerator\n"); return false; }
//
//	collisionBuilder = NvBlastExtAuthoringCreateConvexMeshBuilder(gCooking, &gPhysics->getPhysicsInsertionCallback());
//	if (!collisionBuilder) { printf("Can't create collisionBuilder\n"); return false; }
//
//	collisionParameter.maximumNumberOfHulls = 1;
//	collisionParameter.voxelGridResolution = 0;
//
//	NvBlastTkFrameworkCreate();
//
//	return true;
//}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
#ifdef CONSOLEDEBUG
		AllocConsole();
		freopen("conin$", "r", stdin);
		freopen("conout$", "w", stdout);
		freopen("conout$", "w", stderr);

		printf("NvBlastExtUnity Starting...\n");
#endif
		//if (!Init()) printf("--- ERRORS FOUND! ---\n");
		break;

	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		//TODO: Release Globals
		break;
	}

	return TRUE;
}
#pragma endregion